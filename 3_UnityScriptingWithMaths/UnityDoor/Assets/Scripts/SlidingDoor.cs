﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SlidingDoor : MonoBehaviour
{

    public Transform door1Tran;
    public Transform door2Tran;

    //Floats (or decimal numbers) affecting speed and amount door opens by
    public float speed = 1f;
    public float openAmount = 1f;

    // Use this for initialization
    void Start()
    {

    }
    
    //Makes the door open when player is in range
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            //Old Code door1Tran.localPosition = new Vector3(openAmount, 0, 0);
            //Old Code door2Tran.localPosition = new Vector3(-openAmount, 0, 0);
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", openAmount);
        }
    }

    //Makes the door close when player is out of range
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }
    //Makes the door run smoothly

    IEnumerator DoorMove(float target)

    {
        float xPos = door1Tran.localPosition.x;
        while (xPos < (target - 0.02f)|| xPos > (target+0.02f))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);

            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            yield return null;
            Debug.Log("Opening the door");
        }

        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Finished door opening");
        yield return null;
    }
        

}
