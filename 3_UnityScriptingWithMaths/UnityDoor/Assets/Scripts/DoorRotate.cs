﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DoorRotate : MonoBehaviour
{

    public GameObject door;
    GameObject myPlayer;
    public float targetRot = 90f;
    private Vector3 closedRot;
    bool doorOpen;
    public float speed = 3.5f;
    public MouseListen mouse;
    bool inTrigger;
    bool click;
    public Image cursorImage;






    // Use this for initialization
    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        closedRot = door.transform.localRotation.eulerAngles;

    }

    // Update is called once per frame
    void Update()

    {
        if (inTrigger)

        {
            if (mouse.mouseCursorOn == true)
            {


                if (cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;
                }
            }

            else
            {
                cursorImage.enabled = false;
            }
        }

        if (inTrigger && mouse.mouseClicked && click == false)
        {
            click = true;
            DoorInteract();
        }

        else if (mouse.mouseClicked==false)
        {
            click = false;

        }
    }



    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            cursorImage.enabled = true;
            Debug.Log("Trigger Entered");
            inTrigger = true;
         
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            cursorImage.enabled = false;
            inTrigger = false;

            Debug.Log("Trigger Left");
           
        }

    }

    void DoorInteract()

    {
        Vector3 finishRot;

        if (doorOpen != true)
        {
            Vector3 playerDir = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(playerDir, transform.forward);
            Debug.Log(dot);
            doorOpen = true;

            //When using rotations use quaternion.Euler to make door run smoothly

            if (dot > 0)
            {
                finishRot = (new Vector3(closedRot.x, closedRot.y + targetRot, 0));

            }

            else
            {
                finishRot = (new Vector3(closedRot.x, closedRot.y - targetRot, 0));
            }

        }

        else
        {
            finishRot = (closedRot);
            doorOpen = false;
        }

        StopCoroutine("DoorMotion");
        StartCoroutine("DoorMotion", finishRot);

    }


    //Coroutine - used to slow the door down


    IEnumerator DoorMotion(Vector3 target)
    {
        while (Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) >= 0.02f)
        {
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), speed * Time.deltaTime);
            yield return null;
        }
        door.transform.localRotation = Quaternion.Euler(target);
        yield return null;



    }
}

