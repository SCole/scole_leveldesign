﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public TriggerListener trigger;
    public Image CursorImage;
    
    //Set up variable and get reference to light
    public Light spotlight;

    //Set up variable and get reference to sound
    public AudioSource audioSource;

    //Set up variable and get reference to animation
    public Animation anim;



    // Use this for initialization
    void Start()
    {
        CursorImage.enabled = false;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();

        Debug.Log("Now AutoComplete Works, after much hardship");
    }
    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update was called");

    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (CursorImage.enabled != true)
            {
                CursorImage.enabled = true;
            }


            Debug.Log("Mouse was on me");
        }
        else
        {
            CursorImage.enabled = false;
        }
    }

    void OnMouseExit()
    {

        if (CursorImage.enabled == true)
        {
            CursorImage.enabled = false;
        }

    }
    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audioSource.Play();
            anim.Stop();
            anim.Play();

            Debug.Log("Switch Pressed");
            if (spotlight.intensity > 0f)

            {spotlight.intensity = 0f; }

            else
            { spotlight.intensity = 5f;}
        
        }
    }

    

        
        
        
       
       
        
	    

	
}