﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerListener : MonoBehaviour
{
    public bool playerEntered = false;

    void OnTriggerEnter(Collider col)

    {if (col.gameObject.tag == "Player")
        {
            playerEntered = true;

            Debug.Log("I entered");

        }

    }

    void OnTriggerExit(Collider col)

    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;

            Debug.Log("I left");

        }

    }
}