﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {
    public Animation hingehere;
    public TriggerListener_Com doorTrigger;
    public bool MouseOn = false;

    private void Start()
    {

    }
    private void OnMouseOver()
    {
        if (doorTrigger.playerEntered == true)
        {
            MouseOn = true;
        }

            
    }
    private void OnMouseExit()
    {
        if (doorTrigger.playerEntered == true)
        {
            MouseOn = false;
        }
    }

    private void OnMouseDown()
    {
        if (MouseOn == true)
        {
            Debug.Log("On Me");
            hingehere.Play();
        }
    }

}
