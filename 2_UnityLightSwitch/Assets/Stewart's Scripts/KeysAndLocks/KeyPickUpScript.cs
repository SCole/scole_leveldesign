﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyPickUpScript : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image CursorImage;
    public Text ScreenText;
    public MeshRenderer meshRend;
    public AudioSource AudioKey;
    public bool KeyPickedUp = false;
    public MeshCollider meshCol;
    public Component doorcolliderhere;

    //Use this for Initialisation
    void Start() {
        


        
    }
    void OnTriggerStay()
    {
        if (Input.GetKey(KeyCode.E))

            doorcolliderhere.GetComponent<BoxCollider>().enabled = true;
 
    }
    // Update is called once per frame
    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
           if(CursorImage.enabled==false)
            {
                CursorImage.enabled = true;
                ScreenText.text = "Key";
            }
        }
        else
        {
            CursorImage.enabled = false;
            ScreenText.text = null; 


        }

    }

    private void OnMouseExit()
    {
        if (CursorImage.enabled == true)
        {
            CursorImage.enabled = false;
            ScreenText.text = null;
        }

    }

    void OnMouseDown()
    {
        if(trigger.playerEntered==true)
        {
            AudioKey.Play();
            KeyPickedUp = true;
            meshRend.enabled = false;
            meshCol.enabled = false;
            Invoke("KeyAquiredText", 0.05f);
            Invoke("BlankText", 2f);


        }

    }

    void KeyAquiredText()
    {
        ScreenText.text = "You got a Key!";
    }

    void BlankText()
    {
        ScreenText.text = null;
    }

}
