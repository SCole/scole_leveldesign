﻿using UnityEngine;
using System.Collections;

public class PickUpObject : MonoBehaviour
{
    public Transform player;
    public float throwForce = 10;
    bool hasPlayer = false;
    bool beingCarried = false;
    Rigidbody rBody;

    private void Start()
    {
       rBody = GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider other)
    {
        hasPlayer = true;
    }

    void OnTriggerExit(Collider other)
    {
        hasPlayer = false;
    }

    void Update()
    {
        if (beingCarried)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rBody.isKinematic = false;
                transform.parent = null;
                beingCarried = false;
                rBody.AddForce(Vector3.forward * throwForce);
                
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && hasPlayer)
            {
                rBody.isKinematic = true;
                transform.parent = player;
                beingCarried = true;
            }
        }
    }
}