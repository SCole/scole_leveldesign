﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerScript : MonoBehaviour
{
    public Text InstructionText;

	// Use this for initialization
	void Start ()
    {
        Invoke("IntroText1", 3);
	}
	
    void IntroText1 ()
    {
        InstructionText.text = "Press Space to jump";
        Invoke("IntroText2", 4);
    }

    void IntroText2()
    {
        InstructionText.text = "Left Click to interract with objects";
        Invoke("BlankText", 4);
    }

    void BlankText()
    {
        InstructionText.text = null;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
